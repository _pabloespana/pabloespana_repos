import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { AutomapperModule } from '@automapper/nestjs';
import { classes } from '@automapper/classes'


import { AppController } from './app.controller';
import { AppService } from './app.service';

import { DatabaseModule } from './database/database.module';
import { OrganizationModule } from './organization/organization.module';
import { TribeModule } from './tribe/tribe.module';
import { ReposModule } from './repos/repos.module';


@Module({
  
  imports: [
    ConfigModule.forRoot(),
    AutomapperModule.forRoot({
      strategyInitializer: classes()
    }),
    DatabaseModule,
    OrganizationModule,
    TribeModule,
    ReposModule,
    JwtModule.register({
      secret: 'my-secret',
      signOptions: { expiresIn: '24h' },
    }),
  ],
  controllers: [ AppController ],
  providers: [ AppService ],

})

export class AppModule { }
