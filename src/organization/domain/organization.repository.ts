import { Organization, OrganizationCreate, OrganizationRead, OrganizationUpdate } from "../../organization/domain/organization";

export interface OrganizationRepository {  

    findAll(): Promise<OrganizationRead[]>;

  
    create(organization: OrganizationCreate): Promise<void>;
  
  
    update(id: number, organization: OrganizationUpdate): Promise<void>;
  
  
    delete(id: number): Promise<void>;

}

export const OrganizationRepository = Symbol('OrganizationRepository');