export class OrganizationBase {}


export class Organization extends OrganizationBase {

    id: number;

    name: string;

    status: number;

}

export class OrganizationRead extends OrganizationBase {

    id: number;

    name: string;

    status: number;

}

export class OrganizationCreate extends OrganizationBase {

    name: string;

    status: number;

}


export class OrganizationUpdate extends OrganizationBase {

    name: string;

    status: number;

}