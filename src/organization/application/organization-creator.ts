import { Inject, Injectable } from "@nestjs/common";
import { Organization, OrganizationCreate } from "../../organization/domain/organization";
import { OrganizationRepository } from "../../organization/domain/organization.repository";

@Injectable()
export class OrganizationCreator {
    
    constructor(
        @Inject(OrganizationRepository) private organizationRepository: OrganizationRepository,
    ){}
    
    async create(organization: OrganizationCreate): Promise<void>{
        const newOrganization: OrganizationCreate = new Organization();
        newOrganization.name = organization.name;
        newOrganization.status = organization.status;
        return await this.organizationRepository.create(newOrganization);
    }
}