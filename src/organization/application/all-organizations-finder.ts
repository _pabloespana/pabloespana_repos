import { Inject, Injectable } from "@nestjs/common";
import { OrganizationRead } from "../../organization/domain/organization";
import { OrganizationRepository } from "../../organization/domain/organization.repository";

@Injectable()
export class AllOrganizationsFinder {
    
    constructor(
        @Inject(OrganizationRepository) private organizationRepository: OrganizationRepository,
    ){}
    
    async find(): Promise<OrganizationRead[]>{
        return await this.organizationRepository.findAll();
    }
}