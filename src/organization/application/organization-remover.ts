import { Inject, Injectable } from "@nestjs/common";
import { OrganizationRepository } from "../domain/organization.repository";

@Injectable()
export class OrganizationRemover {
    
    constructor(
        @Inject(OrganizationRepository) private organizationRepository: OrganizationRepository,
    ){}
    
    async remove(id: number): Promise<void>{
        return await this.organizationRepository.delete(id);
    }
}