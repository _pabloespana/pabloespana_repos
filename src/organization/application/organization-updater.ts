import { Inject, Injectable } from "@nestjs/common";
import { OrganizationUpdate } from "../../organization/domain/organization";
import { OrganizationRepository } from "../../organization/domain/organization.repository";

@Injectable()
export class OrganizationUpdater {
    
    constructor(
        @Inject(OrganizationRepository) private organizationRepository: OrganizationRepository,
    ){}
    
    async update(id: number, organization: OrganizationUpdate): Promise<void>{
        return await this.organizationRepository.update(id, organization);
    }
}