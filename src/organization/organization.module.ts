import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { OrganizationCreator } from './application/organization-creator';
import { AllOrganizationsFinder } from './application/all-organizations-finder';
import { OrganizationUpdater } from './application/organization-updater';
import { OrganizationRemover } from './application/organization-remover';
import { OrganizationRepository } from './domain/organization.repository';
import { OrganizationController } from './infraestructure/api/organization.controller';
import { OrganizationRepositoryImpl } from './infraestructure/repositories/organization.repository';
import { OrganizationProfile } from './infraestructure/mapper/organization.profile';

@Module({
  imports: [ DatabaseModule ],
  controllers: [OrganizationController],
  providers: [ 
    OrganizationCreator,
    AllOrganizationsFinder,
    OrganizationUpdater,
    OrganizationRemover,
    OrganizationProfile,
    {
        provide: OrganizationRepository,
        useClass: OrganizationRepositoryImpl
    }
 ],
 exports: [ ]
})

export class OrganizationModule {}