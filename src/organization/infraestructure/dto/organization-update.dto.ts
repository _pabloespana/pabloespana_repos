import { OrganizationCreateDTO } from "./organization-create.dto";

export class OrganizationUpdateDTO extends OrganizationCreateDTO { }