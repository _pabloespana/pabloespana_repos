import { AutoMap } from "@automapper/classes";

export class OrganizationReadDTO {

    @AutoMap()
    id: number;

    @AutoMap()
    name: string;
    
    @AutoMap()
    status: number;

}