import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";


export class OrganizationCreateDTO {

    @ApiProperty()
    @AutoMap()
    name: string;
    
    @ApiProperty()
    @AutoMap()
    status: number;

}