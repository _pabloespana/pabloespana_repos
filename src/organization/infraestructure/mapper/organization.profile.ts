import { AutomapperProfile, InjectMapper } from "@automapper/nestjs";
import { createMap, forMember, ignore, mapFrom, Mapper, MappingProfile } from "@automapper/core";
import { Injectable } from "@nestjs/common";
import { OrganizationEntity } from "../entities/organization.entity";
import { OrganizationCreateDTO } from "../dto/organization-create.dto";
import { OrganizationReadDTO } from "../dto/organization-read.dto";
import { OrganizationUpdateDTO } from "../dto/organization-update.dto";

@Injectable()
export class OrganizationProfile extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }

  override get profile() {
    return (mapper) => {
      createMap(mapper, OrganizationEntity, OrganizationReadDTO);
      createMap(mapper, OrganizationCreateDTO, OrganizationEntity, forMember((dest) => dest.id, ignore()));
      createMap(mapper, OrganizationUpdateDTO, OrganizationEntity);
    };
  }
  
}