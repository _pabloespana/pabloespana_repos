import { Body, Controller, Delete, Get, Inject, Logger, Param, Post, Put, Response, HttpStatus, Res  }
 from '@nestjs/common';
import { OrganizationCreator } from '../../application/organization-creator';
import { AllOrganizationsFinder } from '../../application/all-organizations-finder';
import { OrganizationUpdater } from '../../application/organization-updater';
import { OrganizationRemover } from '../../application/organization-remover';
import { OrganizationReadDTO } from '../dto/organization-read.dto';
import { OrganizationCreateDTO } from '../dto/organization-create.dto';
import { OrganizationUpdateDTO } from '../dto/organization-update.dto';


@Controller('api/v1/organizations')
export class OrganizationController {
    
    constructor(
        @Inject(OrganizationCreator) private organizationCreator: OrganizationCreator,
        @Inject(AllOrganizationsFinder) private allOrganizationsFinder: AllOrganizationsFinder,
        @Inject(OrganizationUpdater) private organizationUpdater: OrganizationUpdater,
        @Inject(OrganizationRemover) private organizationRemover: OrganizationRemover,
    ) {}

    @Get()
    async findAll(@Res() response): Promise<OrganizationReadDTO[]>{
        const organizations: OrganizationReadDTO[] = await this.allOrganizationsFinder.find();
        return response.status(HttpStatus.OK).send(organizations);
    }

    @Post()
    async create(@Body() organization: OrganizationCreateDTO, @Res() response): Promise<void>{
        await this.organizationCreator.create(organization);
        return response.status(HttpStatus.CREATED).send({ message: "Organization successfully created!" });
    }

    @Put('/:id')
    async update(@Param('id') id: number, @Body() organization: OrganizationUpdateDTO, @Res() response): Promise<void>{
        await this.organizationUpdater.update(id, organization);
        return response.status(HttpStatus.CREATED).send({ message: "Organization successfully updated!" });
    }

    @Delete('/:id')
    async delete(@Param('id') id: number, @Res() response): Promise<void>{
        await this.organizationRemover.remove(id);
        return response.status(HttpStatus.OK).send({ message: "Organization successfully removed!" });
    }

}