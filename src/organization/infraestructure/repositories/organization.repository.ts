import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OrganizationRepository } from '../../../organization/domain/organization.repository';
import { OrganizationReadDTO } from '../dto/organization-read.dto';
import { OrganizationCreateDTO } from '../dto/organization-create.dto';
import { OrganizationUpdateDTO } from '../dto/organization-update.dto';
import { OrganizationEntity } from '../entities/organization.entity';


@Injectable()
export class OrganizationRepositoryImpl implements OrganizationRepository {

    constructor(
        @InjectRepository(OrganizationEntity)
        private readonly organizationRepository: Repository<OrganizationEntity>,
      ) {}
    
    async findAll(): Promise<OrganizationReadDTO[]> {
        return this.organizationRepository.find();
    }

    async create(organization: OrganizationCreateDTO): Promise<void> {
        this.organizationRepository.save(organization);
    }

    async update(id: number, organization: OrganizationUpdateDTO): Promise<void> {
        this.organizationRepository.update({ id }, organization);
    }

    async delete(id: number): Promise<void> {
        this.organizationRepository.delete({id})
    }

}