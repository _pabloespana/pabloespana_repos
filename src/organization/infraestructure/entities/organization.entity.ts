import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToOne, JoinColumn, OneToMany } 
from 'typeorm';
import { IsString, IsNotEmpty, IsDate } from 'class-validator';
import { AutoMap } from "@automapper/classes";
import { TribeEntity } from '../../../tribe/infraestructure/entities/tribe.entity';

@Entity({ name: 'organizations' })
export class OrganizationEntity {

  @AutoMap()
  @PrimaryGeneratedColumn()
  id: number;

  @AutoMap()
  @Column()
  name: string;

  @AutoMap()
  @Column()
  status: number;

  @OneToMany(() => TribeEntity, tribe => tribe.organization)
  tribes: TribeEntity[];

}