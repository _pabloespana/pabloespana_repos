import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { OrganizationEntity } from '../organization/infraestructure/entities/organization.entity';
import { TribeEntity } from '../tribe/infraestructure/entities/tribe.entity';
import { ReposEntity } from '../repos/infrestructure/entities/repos.entity';
import { MetricEntity } from '../repos/infrestructure/entities/metric.entity';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
        type: 'cockroachdb',
        host: process.env.DB_HOST.toString(),
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        ssl: true,
        extra: {
          options: process.env.DB_OPTIONS
        },
        entities: [ 
          OrganizationEntity,
          TribeEntity,
          ReposEntity,
          MetricEntity
        ],
        //synchronize: true,
        //autoLoadEntities: true,
        //keepConnectionAlive: true
    }),
    TypeOrmModule.forFeature([
      OrganizationEntity, TribeEntity, ReposEntity, MetricEntity
    ]),
  ],
  exports: [ TypeOrmModule ],
})
export class DatabaseModule {}