import { DataSource } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { config } from 'dotenv';
import { OrganizationEntity } from '../organization/infraestructure/entities/organization.entity';
import { TribeEntity } from '../tribe/infraestructure/entities/tribe.entity';
import { ReposEntity } from '../repos/infrestructure/entities/repos.entity';
import { MetricEntity } from '../repos/infrestructure/entities/metric.entity';
 
config();
 
const configService = new ConfigService();
 
export default new DataSource({
    type: 'cockroachdb',
    host: configService.get('DB_HOST'),
    port: configService.get('DB_PORT'),
    username: configService.get('DB_USERNAME'),
    password: configService.get('DB_PASSWORD'),
    database: configService.get('DB_NAME'),
    ssl: true,
    extra: {
        options: configService.get('DB_OPTIONS'),
    },
    entities: [OrganizationEntity, TribeEntity, ReposEntity, MetricEntity],
    synchronize: false,
    migrationsRun: false,
    migrations: [__dirname + '/migrations/**/*.ts'],
});