import { MigrationInterface, QueryRunner } from "typeorm";

export class ChangeRelationship21665771198125 implements MigrationInterface {
    name = 'ChangeRelationship21665771198125'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "repositories" DROP CONSTRAINT "FK_943a2bc6fad79882d9c76297c56"`);
        await queryRunner.query(`DROP INDEX "repositories"@"IDX_943a2bc6fad79882d9c76297c5" CASCADE`);
        await queryRunner.query(`ALTER TABLE "repositories" RENAME COLUMN "tribe" TO "tribeId"`);
        await queryRunner.query(`CREATE INDEX "IDX_d80cbbd68d9e9618654b379351" ON "repositories" ("tribeId") `);
        await queryRunner.query(`ALTER TABLE "repositories" ADD CONSTRAINT "FK_d80cbbd68d9e9618654b379351f" FOREIGN KEY ("tribeId") REFERENCES "tribes"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "repositories" DROP CONSTRAINT "FK_d80cbbd68d9e9618654b379351f"`);
        await queryRunner.query(`DROP INDEX "repositories"@"IDX_d80cbbd68d9e9618654b379351" CASCADE`);
        await queryRunner.query(`ALTER TABLE "repositories" RENAME COLUMN "tribeId" TO "tribe"`);
        await queryRunner.query(`CREATE INDEX "IDX_943a2bc6fad79882d9c76297c5" ON "repositories" ("tribe") `);
        await queryRunner.query(`ALTER TABLE "repositories" ADD CONSTRAINT "FK_943a2bc6fad79882d9c76297c56" FOREIGN KEY ("tribe") REFERENCES "tribes"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
