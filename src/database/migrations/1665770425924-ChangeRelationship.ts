import { MigrationInterface, QueryRunner } from "typeorm";

export class ChangeRelationship1665770425924 implements MigrationInterface {
    name = 'ChangeRelationship1665770425924'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE SEQUENCE "metrics_id_seq"`);
        await queryRunner.query(`CREATE TABLE "metrics" ("id" INT DEFAULT nextval('"metrics_id_seq"') NOT NULL, "coverage" int8 NOT NULL, "bugs" int8 NOT NULL, "vulnerabilities" int8 NOT NULL, "hotspot" int8 NOT NULL, "code_smells" int8 NOT NULL, "repositoryId" int8, CONSTRAINT "REL_cf636682dc66c14d3240a1729e" UNIQUE ("repositoryId"), CONSTRAINT "PK_5283cad666a83376e28a715bf0e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_cf636682dc66c14d3240a1729e" ON "metrics" ("repositoryId") `);
        await queryRunner.query(`CREATE SEQUENCE "repositories_id_seq"`);
        await queryRunner.query(`CREATE TABLE "repositories" ("id" INT DEFAULT nextval('"repositories_id_seq"') NOT NULL, "name" varchar NOT NULL, "state" varchar NOT NULL, "create_time" timestamptz NOT NULL DEFAULT now(), "status" int8 NOT NULL, "tribe" int8, CONSTRAINT "PK_ef0c358c04b4f4d29b8ca68ddff" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_943a2bc6fad79882d9c76297c5" ON "repositories" ("tribe") `);
        await queryRunner.query(`CREATE SEQUENCE "tribes_id_seq"`);
        await queryRunner.query(`CREATE TABLE "tribes" ("id" INT DEFAULT nextval('"tribes_id_seq"') NOT NULL, "name" varchar NOT NULL, "status" int8 NOT NULL, "organizationId" int8, CONSTRAINT "PK_1a548c615b0edfa360875349896" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_2c0417bea0bad8fbb079251be4" ON "tribes" ("organizationId") `);
        await queryRunner.query(`CREATE SEQUENCE "organizations_id_seq"`);
        await queryRunner.query(`CREATE TABLE "organizations" ("id" INT DEFAULT nextval('"organizations_id_seq"') NOT NULL, "name" varchar NOT NULL, "status" int8 NOT NULL, CONSTRAINT "PK_6b031fcd0863e3f6b44230163f9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "metrics" ADD CONSTRAINT "FK_cf636682dc66c14d3240a1729e8" FOREIGN KEY ("repositoryId") REFERENCES "repositories"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "repositories" ADD CONSTRAINT "FK_943a2bc6fad79882d9c76297c56" FOREIGN KEY ("tribe") REFERENCES "tribes"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "tribes" ADD CONSTRAINT "FK_2c0417bea0bad8fbb079251be4d" FOREIGN KEY ("organizationId") REFERENCES "organizations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "tribes" DROP CONSTRAINT "FK_2c0417bea0bad8fbb079251be4d"`);
        await queryRunner.query(`ALTER TABLE "repositories" DROP CONSTRAINT "FK_943a2bc6fad79882d9c76297c56"`);
        await queryRunner.query(`ALTER TABLE "metrics" DROP CONSTRAINT "FK_cf636682dc66c14d3240a1729e8"`);
        await queryRunner.query(`DROP TABLE "organizations"`);
        await queryRunner.query(`DROP SEQUENCE "organizations_id_seq"`);
        await queryRunner.query(`DROP INDEX "tribes"@"IDX_2c0417bea0bad8fbb079251be4" CASCADE`);
        await queryRunner.query(`DROP TABLE "tribes"`);
        await queryRunner.query(`DROP SEQUENCE "tribes_id_seq"`);
        await queryRunner.query(`DROP INDEX "repositories"@"IDX_943a2bc6fad79882d9c76297c5" CASCADE`);
        await queryRunner.query(`DROP TABLE "repositories"`);
        await queryRunner.query(`DROP SEQUENCE "repositories_id_seq"`);
        await queryRunner.query(`DROP INDEX "metrics"@"IDX_cf636682dc66c14d3240a1729e" CASCADE`);
        await queryRunner.query(`DROP TABLE "metrics"`);
        await queryRunner.query(`DROP SEQUENCE "metrics_id_seq"`);
    }

}
