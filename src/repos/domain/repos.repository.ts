import { Tribe } from "../../tribe/domain/tribe";
import { Repos, ReposCreate, ReposWithMetricRead } from "../../repos/domain/repos";
import { Metric, MetricCreate } from "./metric";

export interface ReposRepository {  
  
    create(repos: ReposCreate): Promise<Repos>;

    getByTribe(id: Tribe): Promise<ReposWithMetricRead[]>; // get respos by tribes with repos, org and metric info

    createMetric(metric: MetricCreate): Promise<void>;

    getMetrics(id: Repos): Promise<Metric>;
  
}

export const ReposRepository = Symbol('ReposRepository');