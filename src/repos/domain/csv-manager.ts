
export interface CSVManager {  
  
    create(content: any): Promise<any>;
  
}

export const CSVManager = Symbol('CSVManager');