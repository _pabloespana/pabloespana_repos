import { Tribe } from "../../tribe/domain/tribe";
import { Metric } from "./metric";


export class ReposBase {}


export class Repos extends ReposBase {

    id: number;

    tribe: Tribe;

    name: string;

    state: string;

    createTime: string;

    status: number

}


export class ReposRead extends ReposBase {

    id: number;

    tribe: Tribe;

    name: string;

    state: string;

    createTime: string;

    status: number

}


export class ReposCreate extends ReposBase{
    
    tribe: Tribe;

    name: string;

    state: string;

    status: number;

}


export class ReposWithStateRead extends ReposBase {

    id: number;

    state: string;

}

export class ReposWithMetricRead extends ReposBase {

    id: number;

    tribe: string;

    organization: string;

    coverage: string;

    codeSmells: number;

    bugs: number;

    vulnerabilities: number;

    hotspots: number;

    metrics: Metric;

    name: string;

    verificationState: string;

    state: string;

}