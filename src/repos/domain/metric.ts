import { Repos } from "./repos";

export class MetricBase {}


export class Metric extends MetricBase {

    id: number;

    repository: Repos;

    coverage: number;

    bugs: number;

    vulnerabilities: number;

    hotspot: number;

    codeSmells: number;

}

export class MetricRead extends MetricBase {

    id: number;

    repository: Repos;

    coverage: number;

    bugs: number;

    vulnerabilities: number;

    hotspot: number;

    codeSmells: number;

}

export class MetricCreate extends MetricBase{
    
    repository: Repos;

    coverage: number;

    bugs: number;

    vulnerabilities: number;

    hotspot: number;

    codeSmells: number;

}