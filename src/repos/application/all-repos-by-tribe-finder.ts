import { Inject, Injectable } from "@nestjs/common";
import { ReposWithMetricRead } from "../../repos/domain/repos";
import { ReposRepository } from "../../repos/domain/repos.repository";

@Injectable()
export class AllReposByTribeFinder {
    
    constructor(
        @Inject(ReposRepository) private reposRepository: ReposRepository,
    ){}
    
    async find(tribe): Promise<ReposWithMetricRead[]>{
        return await this.reposRepository.getByTribe(tribe);
    }

}