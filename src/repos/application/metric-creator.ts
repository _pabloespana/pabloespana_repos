import { Inject, Injectable } from "@nestjs/common";
import { ReposRepository } from "../../repos/domain/repos.repository";
import { Metric } from "../domain/metric";

@Injectable()
export class MetricCreator {
    
    constructor(
        @Inject(ReposRepository) private reposRepository: ReposRepository,
    ){}
    
    async create(repos): Promise<void>{
        const newMetric: Metric = new Metric();
        newMetric.bugs = 0;
        newMetric.codeSmells = 1;
        newMetric.coverage = 80;
        newMetric.hotspot = 0;
        newMetric.repository = repos;
        newMetric.vulnerabilities = 2;
        return await this.reposRepository.createMetric(newMetric);
    }

}