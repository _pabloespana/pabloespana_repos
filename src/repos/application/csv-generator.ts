import { Inject, Injectable } from "@nestjs/common";
import { CSVManager } from '../domain/csv-manager'
import { ReposWithMetricRead } from "../domain/repos";

@Injectable()
export class CSVGenerator {
    
    constructor(
        @Inject(CSVManager) private CSVFileManager: CSVManager,
    ){}
    
    async generate(repos: ReposWithMetricRead[]): Promise<any>{
        let data = `id,name,tribe,organization,coverage,code_smells,bugs,vulnerabilities,hotspots,state
        `;

        for (let index = 0; index < repos.length; index++) {
            data+=`${repos[index].id},${repos[index].name},${repos[index].tribe},${repos[index].organization},${repos[index].coverage},${repos[index].codeSmells},${repos[index].bugs},${repos[index].vulnerabilities},${repos[index].hotspots},${repos[index].state}
            `
        }
        
        return await this.CSVFileManager.create(data);
    }

}