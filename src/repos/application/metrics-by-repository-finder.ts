import { Inject, Injectable } from "@nestjs/common";
import { ReposRepository } from "../../repos/domain/repos.repository";
import { Metric, MetricRead } from "../domain/metric";

@Injectable()
export class MetricsByRepositoryFinder {
    
    constructor(
        @Inject(ReposRepository) private reposRepository: ReposRepository,
    ){}
    
    async find(repos): Promise<MetricRead>{
        return await this.reposRepository.getMetrics(repos);
    }

}