import { Inject, Injectable } from "@nestjs/common";
import { Repos, ReposCreate } from "../../repos/domain/repos";
import { ReposRepository } from "../../repos/domain/repos.repository";

@Injectable()
export class ReposCreator {
    
    constructor(
        @Inject(ReposRepository) private reposRepository: ReposRepository,
    ){}
    
    async create(repos: ReposCreate): Promise<Repos>{
        const newRepos: Repos = new Repos();
        newRepos.name = repos.name;
        newRepos.tribe = repos.tribe;
        newRepos.state = repos.state;
        newRepos.status = repos.status;
        return await this.reposRepository.create(newRepos);
    }

}