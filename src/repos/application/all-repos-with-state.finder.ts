import { Inject, Injectable } from "@nestjs/common";
import { ReposWithStateRead } from "../../repos/domain/repos";

@Injectable()
export class AllReposWithStateFinder {

    async find(): Promise<ReposWithStateRead[]>{

        const repositories: ReposWithStateRead[] = [
            {
                id: 1,
                state: '604'
            },
            {
                id: 2,
                state: '605'
            },
            {
                id: 3,
                state: '606'
            }
        ]

        return repositories;
    }

}