import { AutomapperProfile, InjectMapper } from "@automapper/nestjs";
import { createMap, forMember, mapFrom, Mapper } from "@automapper/core";
import { Injectable } from "@nestjs/common";
import { ReposEntity } from "../entities/repos.entity";
import { ReposCreateDTO } from "../dto/repos-create.dto";
import { ReposReadDTO } from "../dto/repos-read.dto";
import { MetricCreateDTO } from "../dto/metric-create.dto";
import { MetricEntity } from "../entities/metric.entity";
import { ReposWithMetricReadDTO } from "../dto/repos-with-metrics-read.dto";

@Injectable()
export class ReposProfile extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }

  override get profile() {
    return (mapper) => {
      createMap(mapper, ReposEntity, ReposReadDTO);
      createMap(mapper, ReposEntity, ReposWithMetricReadDTO, 
        forMember(
          dto=>dto.tribe, mapFrom(entity=>entity.tribe.name)
        ),
        forMember(
          dto=>dto.organization, mapFrom(entity=>entity.tribe.organization.name)
        ),
        forMember(
          dto=>dto.coverage, mapFrom(entity=>entity.metrics.coverage.toString() + '%')
        ),
        forMember(
          dto=>dto.codeSmells, mapFrom(entity=>entity.metrics.codeSmells)
        ),
        forMember(
          dto=>dto.bugs, mapFrom(entity=>entity.metrics.bugs)
        ),
        forMember(
          dto=>dto.vulnerabilities, mapFrom(entity=>entity.metrics.vulnerabilities)
        ),
        forMember(
          dto=>dto.hotspots, mapFrom(entity=>entity.metrics.hotspot)
        ),
        forMember(
          dto=>dto.state, mapFrom(entity=>entity.state=="E"?"Enabled":entity.state=="D"?"Disabled":"Archived")
        ),
      );
      createMap(mapper, ReposCreateDTO, ReposEntity, 
        forMember(
          dto=>dto.tribe, mapFrom(entity=>entity.tribe)
        ),
      );
      createMap(mapper, MetricCreateDTO, MetricEntity,
        forMember(
          dto=>dto.repository, mapFrom(entity=>entity.repository)
        ),
      );

    };
  }
  
}