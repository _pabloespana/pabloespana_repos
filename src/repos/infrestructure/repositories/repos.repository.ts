import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, MoreThan, LessThan, Between } from 'typeorm';
import { ReposCreateDTO } from '../dto/repos-create.dto';
import { MetricCreateDTO } from '../dto/metric-create.dto';
import { ReposReadDTO } from '../dto/repos-read.dto';
import { ReposEntity } from '../entities/repos.entity';
import { ReposRepository } from '../../../repos/domain/repos.repository';
import { TribeEntity } from '../../../tribe/infraestructure/entities/tribe.entity';
import { ReposWithMetricReadDTO } from '../dto/repos-with-metrics-read.dto';
import { InjectMapper } from '@automapper/nestjs';
import { Mapper } from '@automapper/core';
import { MetricEntity } from '../entities/metric.entity';
import { MetricReadDTO } from '../dto/metric-read.dto';


@Injectable()
export class ReposRepositoryImpl implements ReposRepository {

    constructor(
        @InjectRepository(ReposEntity) 
        private readonly reposRepository: Repository<ReposEntity>,
        @InjectRepository(MetricEntity) 
        private readonly metricRepository: Repository<MetricEntity>,
        @InjectMapper() private readonly mapper: Mapper,
      ) {}
    
    async findAll(): Promise<ReposReadDTO[]> {
        const result: ReposEntity[] =  await this.reposRepository.find({ where: {  } });
        return await this.mapper.mapArrayAsync(result, ReposEntity, ReposReadDTO);
    }

    async create(repos: ReposCreateDTO): Promise<ReposReadDTO> {
        const data: ReposEntity = await this.mapper.mapAsync(repos, ReposCreateDTO, ReposEntity);
        const newRepos: ReposEntity = this.reposRepository.create(data);
        const reposCreated: ReposEntity = await this.reposRepository.save(newRepos);
        return await this.mapper.mapAsync(reposCreated, ReposEntity, ReposReadDTO);
    }

    async getByTribe(tribe: TribeEntity): Promise<ReposWithMetricReadDTO[]>{
        const result: ReposEntity[] =  await this.reposRepository.find({ 
            where: { 
                tribe: { id: tribe.id }, 
                metrics: { coverage: MoreThan(75)},
                state: 'E',
                createTime: Between(
                    new Date(2021, 12, 31).toISOString(), 
                    new Date(2022, 12, 31).toISOString()
                ),
            },
            relations: ['tribe', 'metrics', 'tribe.organization'],
            loadRelationIds: false
        });
        return await this.mapper.mapArrayAsync(result, ReposEntity, ReposWithMetricReadDTO);
    }
    
    async createMetric(metric: MetricCreateDTO): Promise<void>{
        const data = await this.mapper.mapAsync(metric, MetricCreateDTO, MetricEntity);
        await this.metricRepository.save(data);
    }

    async getMetrics(id): Promise<MetricReadDTO>{        
        return await this.metricRepository.findOne({ where: { repository: {id} }});
    }

}