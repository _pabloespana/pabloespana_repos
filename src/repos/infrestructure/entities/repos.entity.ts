import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToOne, JoinColumn, OneToMany, ManyToOne } 
from 'typeorm';
import { IsString, IsNotEmpty, IsDate } from 'class-validator';
import { AutoMap } from "@automapper/classes";
import { TribeEntity } from '../../../tribe/infraestructure/entities/tribe.entity';
import { MetricEntity } from './metric.entity';

@Entity({ name: 'repositories' })
export class ReposEntity {

  @AutoMap()
  @PrimaryGeneratedColumn()
  id: number;

  @AutoMap()
  @Column()
  name: string;

  @AutoMap()
  @Column()
  state: string;

  @AutoMap()
  @CreateDateColumn({name:'create_time'})
  createTime: string;

  @AutoMap()
  @Column()
  status: number;
  
  @ManyToOne(() => TribeEntity, (tribe) => tribe.repositories)
  @JoinColumn()
  tribe: TribeEntity;

  @OneToOne(() => MetricEntity, (metric) => metric.repository)
  metrics: MetricEntity


}