import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToOne, JoinColumn, OneToMany, ManyToOne } 
from 'typeorm';
import { IsString, IsNotEmpty, IsDate } from 'class-validator';
import { AutoMap } from "@automapper/classes";
import { ReposEntity } from './repos.entity';

@Entity({ name: 'metrics' })
export class MetricEntity {

  @AutoMap()
  @PrimaryGeneratedColumn()
  id: number;

  @AutoMap()
  @Column()
  coverage: number;

  @AutoMap()
  @Column()
  bugs: number;

  @AutoMap()
  @Column()
  vulnerabilities: number;

  @AutoMap()
  @Column()
  hotspot: number;
  
  @AutoMap()
  @Column({name:'code_smells'})
  codeSmells: number;

  @OneToOne(() => ReposEntity, repository => repository.metrics)
  @JoinColumn()
  repository: ReposEntity;

}