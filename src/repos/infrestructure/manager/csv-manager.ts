import { Injectable } from '@nestjs/common';
import { CSVManager } from '../../../repos/domain/csv-manager';
import * as fs from 'fs'

@Injectable()
export class CSVManagerImpl implements CSVManager {

    async create(content: any): Promise<void> {
        fs.writeFile("metrics.csv", content, "utf-8", (err) => {
            if (err) console.log(err);
            else console.log("Data saved");
        });        
    }
}