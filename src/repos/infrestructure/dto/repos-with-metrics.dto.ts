import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";
import { Metric } from "../../domain/metric";
import { Tribe } from "../../../tribe/domain/tribe";

export class ReposWithMetricReadDTO {

    @ApiProperty()
    @AutoMap()
    id: number;

    @ApiProperty()
    @AutoMap()
    tribe: Tribe;

    @ApiProperty()
    @AutoMap()
    name: string;

    @ApiProperty()
    @AutoMap()
    state: string;

    @ApiProperty()
    @AutoMap()
    create_time: string;
    
    @ApiProperty()
    @AutoMap()
    status: number;

    @ApiProperty()
    @AutoMap()
    metrics: Metric;

}