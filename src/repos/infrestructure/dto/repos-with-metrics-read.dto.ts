import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";
import { Metric } from "../../domain/metric";
  
export class ReposWithMetricReadDTO {

    @ApiProperty()
    @AutoMap()
    id: number;

    @ApiProperty()
    @AutoMap()
    tribe: string;

    @ApiProperty()
    @AutoMap()
    organization: string;

    @ApiProperty()
    @AutoMap()
    coverage: string;

    @ApiProperty()
    @AutoMap()
    codeSmells: number;

    @ApiProperty()
    @AutoMap()
    bugs: number;

    @ApiProperty()
    @AutoMap()
    vulnerabilities: number;

    @ApiProperty()
    @AutoMap()
    hotspots: number;
    
    @ApiProperty()
    @AutoMap()
    name: string;

    @ApiProperty()
    @AutoMap()
    verificationState: string;

    @ApiProperty()
    @AutoMap()
    state: string;

    @ApiProperty()
    @AutoMap()
    metrics: Metric;

}