import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";
import { TribeEntity } from "../../../tribe/infraestructure/entities/tribe.entity";

export class ReposCreateDTO {

    @ApiProperty()
    @AutoMap()
    name: string;

    @ApiProperty({ example: 1 })
    @AutoMap()
    tribe: TribeEntity;

    @ApiProperty()
    @AutoMap()
    state: string;
    
    @ApiProperty()
    @AutoMap()
    status: number;

}