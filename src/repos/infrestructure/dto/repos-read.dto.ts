import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";
import { TribeEntity } from "../../../tribe/infraestructure/entities/tribe.entity";

export class ReposReadDTO {

    @ApiProperty()
    @AutoMap()
    id: number;

    @ApiProperty()
    @AutoMap()
    tribe: TribeEntity;

    @ApiProperty()
    @AutoMap()
    name: string;

    @ApiProperty()
    @AutoMap()
    state: string;

    @ApiProperty()
    @AutoMap()
    createTime: string;
    
    @ApiProperty()
    @AutoMap()
    status: number;

}