import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";
import { ReposEntity } from "../entities/repos.entity";

export class MetricReadDTO {

    @ApiProperty()
    @AutoMap()
    id: number;

    @ApiProperty()
    @AutoMap()
    repository: ReposEntity;

    @ApiProperty()
    @AutoMap()
    coverage: number;

    @ApiProperty()
    @AutoMap()
    bugs: number;

    @ApiProperty()
    @AutoMap()
    vulnerabilities: number;
    
    @ApiProperty()
    @AutoMap()
    hotspot: number;

    @ApiProperty()
    @AutoMap()
    codeSmells: number;

}