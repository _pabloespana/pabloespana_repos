import { Body, Controller, Delete, Get, Inject, Logger, Param, Post, Put, Response, HttpStatus, Res, StreamableFile  }
 from '@nestjs/common';
import { ReposCreateDTO } from '../dto/repos-create.dto';
import { ReposCreator } from '../../../repos/application/repos-creator';
import { MetricCreator } from '../../../repos/application/metric-creator';
import { AllReposByTribeFinder } from '../../../repos/application/all-repos-by-tribe-finder';
import { ReposWithStateDTO } from '../dto/repos-with-state.dot';
import { AllReposWithStateFinder } from '../../../repos/application/all-repos-with-state.finder';
import { ReposWithMetricReadDTO } from '../dto/repos-with-metrics-read.dto';
import { TribeByIdFinder } from '../../../tribe/application/tribe-by-id-finder';
import { CSVGenerator } from '../../../repos/application/csv-generator';
import { createReadStream } from 'fs';

@Controller('api/v1/repositories')
export class ReposController {
    
    constructor(
        @Inject(ReposCreator) private reposCreator: ReposCreator,
        @Inject(AllReposWithStateFinder) private allReposWithStateFinder: AllReposWithStateFinder,
        @Inject(AllReposByTribeFinder) private allReposByTribeFinder: AllReposByTribeFinder,
        @Inject(MetricCreator) private metricCreator: MetricCreator,
        @Inject(TribeByIdFinder) private tribeByIdFinder: TribeByIdFinder,
        @Inject(CSVGenerator) private CSVfileGenerator: CSVGenerator,
    ) {}

    @Get('/verification')
    async findAll(@Res() response): Promise<ReposWithStateDTO[]>{
        const repositories: ReposWithStateDTO[] = await this.allReposWithStateFinder.find();
        return response.status(HttpStatus.OK).send(repositories);
    }

    @Get('/:tribe')
    async findByTribe(@Param('tribe') tribe: number, @Res() response): Promise<ReposWithMetricReadDTO[]>{
        const tribeFound = await this.tribeByIdFinder.find(tribe);
        if (!tribeFound.length) return response.status(HttpStatus.NOT_FOUND)
        .send({"error" : 'La Tribu no se encuentra registrada'})

        const repositories: ReposWithMetricReadDTO[] = await this.allReposByTribeFinder.find(tribe);
        if (!repositories.length) return response.status(HttpStatus.NOT_FOUND)
            .send({"error" : 'La Tribu no tiene repositorios que cumplan con la cobertura necesaria'})
        
        return response.status(HttpStatus.OK).send(repositories);
    }


    @Post()
    async create(
        @Body() repos: ReposCreateDTO, 
        @Res() response): Promise<void>
    {
        const { id } = await this.reposCreator.create(repos);
        await this.metricCreator.create(id);
        return response.status(HttpStatus.CREATED).send({ message: "Repositiry successfully created!" });
    }

    @Get('/:tribe/report')
    async generateReport(@Param('tribe') tribe: number, @Res() response): Promise<StreamableFile>{
        const repos = await this.allReposByTribeFinder.find(tribe);
        await this.CSVfileGenerator.generate(repos);
        const file = createReadStream('metrics.csv');
        response.set({'Content-Type': 'application/csv', 'Content-Disposition': 'attachment; filename="metrics.csv"'});
        return file.pipe(response);
    }
  

}