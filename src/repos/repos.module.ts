import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { ReposCreator } from './application/repos-creator';
import { AllReposByTribeFinder } from './application/all-repos-by-tribe-finder';
import { AllReposWithStateFinder } from './application/all-repos-with-state.finder';

import { ReposRepository } from './domain/repos.repository'
import { ReposController } from './infrestructure/api/repos.controller';
import { ReposRepositoryImpl } from './infrestructure/repositories/repos.repository';
import { ReposProfile } from './infrestructure/mapper/repos.profile';
import { MetricCreator } from './application/metric-creator';
import { TribeModule } from '../tribe/tribe.module';
import { MetricsByRepositoryFinder } from './application/metrics-by-repository-finder';
import { CSVGenerator } from './application/csv-generator';
import { CSVManager } from './domain/csv-manager';
import { CSVManagerImpl } from './infrestructure/manager/csv-manager';

@Module({
  imports: [ DatabaseModule, TribeModule ],
  controllers: [ ReposController ],
  providers: [ 
    ReposCreator,
    MetricCreator,
    AllReposByTribeFinder,
    AllReposWithStateFinder,
    ReposProfile,
    MetricsByRepositoryFinder,
    CSVGenerator,
    {
        provide: CSVManager,
        useClass: CSVManagerImpl
    },
    
    {
        provide: ReposRepository,
        useClass: ReposRepositoryImpl
    }
  ],
 exports: [ ]
})

export class ReposModule {}