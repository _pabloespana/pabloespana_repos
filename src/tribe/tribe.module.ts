import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { TribeCreator } from './application/tribe-creator';
import { AllTribesFinder } from './application/all-tribes-finder';
import { TribeRepository } from './domain/tribe.repository'
import { TribeController } from './infraestructure/api/tribe.controller';
import { TribeRepositoryImpl } from './infraestructure/repositories/tribe.repository';
import { TribeProfile } from './infraestructure/mapper/tribe.profile';
import { TribeByIdFinder } from './application/tribe-by-id-finder';

@Module({
  imports: [ DatabaseModule ],
  controllers: [ TribeController ],
  providers: [ 
    TribeCreator,
    AllTribesFinder,
    TribeProfile,
    TribeByIdFinder,
    {
        provide: TribeRepository,
        useClass: TribeRepositoryImpl
    }
 ],
 exports: [ TribeByIdFinder ]
})

export class TribeModule {}