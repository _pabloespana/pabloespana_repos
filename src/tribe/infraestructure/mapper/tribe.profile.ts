import { AutomapperProfile, InjectMapper } from "@automapper/nestjs";
import { createMap, forMember, ignore, mapFrom, Mapper, MappingProfile } from "@automapper/core";
import { Injectable } from "@nestjs/common";
import { TribeEntity } from "../entities/tribe.entity";
import { TribeCreateDTO } from "../dto/tribe-create.dto";
import { TribeReadDTO } from "../dto/tribe-read.dto";

@Injectable()
export class TribeProfile extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }

  override get profile() {
    return (mapper) => {
      createMap(mapper, TribeEntity, TribeReadDTO);
      createMap(mapper, TribeCreateDTO, TribeEntity, forMember((dest) => dest.id, ignore()));
    };
  }
  
  
}