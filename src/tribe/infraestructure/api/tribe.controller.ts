import { Body, Controller, Get, Inject, Post, HttpStatus, Res  }
 from '@nestjs/common';
import { TribeCreator } from '../../application/tribe-creator';
import { AllTribesFinder } from '../../application/all-tribes-finder';
import { TribeReadDTO } from '../dto/tribe-read.dto';
import { TribeCreateDTO } from '../dto/tribe-create.dto';

@Controller('api/v1/tribes')
export class TribeController {
    
    constructor(
        @Inject(TribeCreator) private tribeCreator: TribeCreator,
        @Inject(AllTribesFinder) private allTribesFinder: AllTribesFinder,
    ) {}

    @Get()
    async findAll(@Res() response): Promise<TribeReadDTO[]>{
        const tribes: TribeReadDTO[] = await this.allTribesFinder.find();
        return response.status(HttpStatus.OK).send(tribes);
    }

    @Post()    
    async create(
        @Body() tribe: TribeCreateDTO, 
        @Res() response): Promise<void>
    {
        await this.tribeCreator.create(tribe);
        return response.status(HttpStatus.CREATED).send({ message: "Tribe successfully created!" });
    }
 

}