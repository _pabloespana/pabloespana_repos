import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";
import { Organization } from "../../../organization/domain/organization";

export class TribeReadDTO {

    @ApiProperty()
    @AutoMap()
    id: number;

    @ApiProperty()
    @AutoMap()
    organization: Organization;

    @ApiProperty()
    @AutoMap()
    name: string;
    
    @ApiProperty()
    @AutoMap()
    status: number;

}