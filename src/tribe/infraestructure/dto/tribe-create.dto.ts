import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";
import { Organization } from "../../../organization/domain/organization";

export class TribeCreateDTO {

    @ApiProperty()
    @AutoMap()
    name: string;

    @ApiProperty({ example: 1 })
    @AutoMap()
    organization: Organization;
    
    @ApiProperty()
    @AutoMap()
    status: number;

}