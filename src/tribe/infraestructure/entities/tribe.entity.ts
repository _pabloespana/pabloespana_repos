import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToOne, JoinColumn, OneToMany, ManyToOne } 
from 'typeorm';
import { IsString, IsNotEmpty, IsDate } from 'class-validator';
import { AutoMap } from "@automapper/classes";
import { OrganizationEntity } from '../../../organization/infraestructure/entities/organization.entity';
import { ReposEntity } from '../../../repos/infrestructure/entities/repos.entity';

@Entity({ name: 'tribes' })
export class TribeEntity {

  @AutoMap()
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => OrganizationEntity, (organization) => organization.tribes)
  organization: OrganizationEntity;

  @AutoMap()
  @Column()
  name: string;

  @AutoMap()
  @Column()
  status: number;

  @OneToMany(() => ReposEntity, repository => repository.tribe)
  repositories: ReposEntity[];

}