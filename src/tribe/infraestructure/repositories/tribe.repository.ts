import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TribeRepository } from '../../../tribe/domain/tribe.repository';
import { TribeReadDTO } from '../dto/tribe-read.dto';
import { TribeCreateDTO } from '../dto/tribe-create.dto';
import { TribeEntity } from '../entities/tribe.entity';


@Injectable()
export class TribeRepositoryImpl implements TribeRepository {

    constructor(
        @InjectRepository(TribeEntity)
        private readonly tribeRepository: Repository<TribeEntity>,
      ) {}
    
    async findAll(): Promise<TribeReadDTO[]> {   
        return await this.tribeRepository.find({
            relations: ['organization'],
            loadRelationIds: true
        });
    }

    async findById(id: number): Promise<TribeEntity[]> {
        return await this.tribeRepository.find({ where: { id } });
    }

    async create(tribe: TribeCreateDTO): Promise<void> {
        await this.tribeRepository.save(tribe);
    }

}