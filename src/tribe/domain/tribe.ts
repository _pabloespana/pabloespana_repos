import { Organization } from "../../organization/domain/organization";

export class TribeBase {}


export class Tribe extends TribeBase {

    id: number;

    organization: Organization;

    name: string;

    status: number;

}


export class TribeCreateIn extends TribeBase {

    organization: Organization;

    name: string;

    status: number;

}

