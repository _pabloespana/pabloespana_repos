import { Tribe, TribeCreateIn } from "../../tribe/domain/tribe";

export interface TribeRepository {  

    findAll(): Promise<Tribe[]>;

    findById(id: number): Promise<Tribe[]>;

    create(tribe: TribeCreateIn): Promise<void>;

}

export const TribeRepository = Symbol('TribeRepository');