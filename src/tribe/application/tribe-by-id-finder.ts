import { Inject, Injectable } from "@nestjs/common";
import { Tribe } from "../../tribe/domain/tribe";
import { TribeRepository } from "../../tribe/domain/tribe.repository";

@Injectable()
export class TribeByIdFinder {
    
    constructor(
        @Inject(TribeRepository) private tribeRepository: TribeRepository,
    ){}
    
    async find(id: number): Promise<Tribe[]>{
        return await this.tribeRepository.findById(id);
    }
}