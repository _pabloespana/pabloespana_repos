import { Inject, Injectable } from "@nestjs/common";
import { Tribe, TribeCreateIn } from "../domain/tribe";
import { TribeRepository } from "../domain/tribe.repository";

@Injectable()
export class TribeCreator {
    
    constructor(
        @Inject(TribeRepository) private tribeRepository: TribeRepository,
    ){}
    
    async create(tribe: TribeCreateIn): Promise<void>{
        const newTribe: Tribe = new Tribe();
        newTribe.name = tribe.name;
        newTribe.status = tribe.status;
        newTribe.organization = tribe.organization;
        return await this.tribeRepository.create(newTribe);
    }
}