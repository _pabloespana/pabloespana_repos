import { Inject, Injectable } from "@nestjs/common";
import { Tribe } from "../../tribe/domain/tribe";
import { TribeRepository } from "../../tribe/domain/tribe.repository";

@Injectable()
export class AllTribesFinder {
    
    constructor(
        @Inject(TribeRepository) private tribeRepository: TribeRepository,
    ){}
    
    async find(): Promise<Tribe[]>{
        return await this.tribeRepository.findAll();
    }
}